package ru.shchurin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.shchurin.tm.model.Project;
import ru.shchurin.tm.model.Task;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Repository
public final class TaskRepo {

    @NotNull
    private final Map<String, Task> tasks = new HashMap<>();

    {
        @NotNull final Task task1 = new Task();
        task1.setName("Task1");
        task1.setDescription("Description1");
        tasks.put(task1.getId(), task1);
        @NotNull final Task task2 = new Task();
        task2.setName("Task2");
        task2.setDescription("Description2");
        tasks.put(task2.getId(), task2);
    }

    @NotNull
    public Collection<Task> findAll() {
        return tasks.values();
    }

    @Nullable
    public Task findOne(@NotNull final String taskId) {
        return tasks.get(taskId);
    }

    public void persist(@NotNull final Task task) {
        if (tasks.containsKey(task.getId())) return;
        tasks.put(task.getId(), task);
    }

    public void merge(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public void remove(@NotNull final String taskId) {
        tasks.remove(taskId);
    }

    public void removeAll() {
        tasks.clear();
    }

    public void removeByName(@NotNull final String name) {
        for (Iterator<Map.Entry<String, Task>> it = tasks.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Task> e = it.next();
            if (name.equals(e.getValue().getName())) {
                it.remove();
            }
        }
    }
}
