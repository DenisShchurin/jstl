package ru.shchurin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.shchurin.tm.model.Project;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Repository
public final class ProjectRepo {

    @NotNull
    private final Map<String, Project> projects = new HashMap<>();

    {
        @NotNull final Project project1 = new Project();
        project1.setName("Project1");
        project1.setDescription("Description1");
        projects.put(project1.getId(), project1);
        @NotNull final Project project2 = new Project();
        project2.setName("Project2");
        project2.setDescription("Description2");
        projects.put(project2.getId(), project2);
    }

    @NotNull
    public Collection<Project> findAll() {
        return projects.values();
    }

    @Nullable
    public Project findOne(@NotNull final String projectId) {
        return projects.get(projectId);
    }

    public void persist(@NotNull final Project project) {
        if (projects.containsKey(project.getId())) return;
        projects.put(project.getId(), project);
    }

    public void merge(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void remove(@NotNull final String projectId) {
        projects.remove(projectId);
    }

    public void removeAll() {
        projects.clear();
    }

    public void removeByName(@NotNull final String name) {
        for (Iterator<Map.Entry<String, Project>> it = projects.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Project> e = it.next();
            if (name.equals(e.getValue().getName())) {
                it.remove();
            }
        }
    }
}
