package ru.shchurin.tm.model;


import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public final class Project extends AbstractEntity{

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Nullable
    private Status status = Status.STATUS_SCHEDULED;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
