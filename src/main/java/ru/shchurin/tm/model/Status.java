package ru.shchurin.tm.model;

public enum Status {

    STATUS_SCHEDULED,
    STATUS_IN_PROGRESS,
    STATUS_DONE
}
