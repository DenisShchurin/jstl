package ru.shchurin.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.shchurin.tm.model.Project;
import ru.shchurin.tm.service.ProjectService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
public final class ProjectController {

    @NotNull
    private final ProjectService projectService;

    @Autowired
    public ProjectController(@NotNull final ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping("/")
    public String root() {
        return "redirect:/project-list";
    }

    @GetMapping("/project-list")
    public String projectList(@NotNull final Model model) {
        @Nullable final Collection<Project> projects = projectService.findAll();
        List<Project> projects2 = new ArrayList<>(projects);
        model.addAttribute("projects", projects2);
        return "project-list";
    }

    @GetMapping("/project-view/{id}")
    public String projectView(@NotNull final Model model, @PathVariable("id") final String id) {
        @Nullable final Project project = projectService.findOne(id);
        model.addAttribute("project", project);
        return "project";
    }

    @GetMapping("/project-create")
    public String projectCreate(@NotNull final Model model) {
        @NotNull final Project project = new Project();
        model.addAttribute("project", project);
        return "project-create";
    }

    @GetMapping("/project-edit/{id}")
    public String projectEdit(@NotNull final Model model, @PathVariable("id") final String id) {
        @Nullable final Project project = projectService.findOne(id);
        model.addAttribute("project", project);
        return "project-edit";
    }

    @PostMapping("/project-save")
    public String projectSave(@ModelAttribute("project") final Project project) {
        projectService.persist(project);
        System.out.println(projectService.findAll().size());
        return "redirect:/project-list";
    }

    @PostMapping("/project-delete/{id}")
    public String projectRemove(@NotNull final Model model, @PathVariable("id") final String id) {
        projectService.remove(id);
        return "redirect:/project-list";
    }
}
