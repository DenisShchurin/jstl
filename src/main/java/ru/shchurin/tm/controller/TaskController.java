package ru.shchurin.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.shchurin.tm.model.Project;
import ru.shchurin.tm.model.Task;
import ru.shchurin.tm.service.ProjectService;
import ru.shchurin.tm.service.TaskService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
public class TaskController {

    @NotNull
    private final TaskService taskService;

    @Autowired
    public TaskController(@NotNull final TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/task-list")
    public String taskList(@NotNull final Model model) {
        @Nullable final Collection<Task> tasks = taskService.findAll();
        model.addAttribute("tasks", tasks);
        return "task-list";
    }

    @GetMapping("/task-view/{id}")
    public String projectView(@NotNull final Model model, @PathVariable("id") final String id) {
        @Nullable final Task task = taskService.findOne(id);
        model.addAttribute("task", task);
        return "task";
    }

    @GetMapping("/task-create")
    public String taskCreate(@NotNull final Model model) {
        @NotNull final Task task = new Task();
        model.addAttribute("task", task);
        return "task-create";
    }

    @GetMapping("/task-edit/{id}")
    public String taskEdit(@NotNull final Model model, @PathVariable("id") final String id) {
        @Nullable final Task task = taskService.findOne(id);
        model.addAttribute("task", task);
        return "task-edit";
    }

    @PostMapping("/task-save")
    public String taskSave(@ModelAttribute("task") final Task task) {
        taskService.persist(task);
        return "redirect:/task-list";
    }

    @PostMapping("/task-delete/{id}")
    public String taskRemove(@NotNull final Model model, @PathVariable("id") final String id) {
        taskService.remove(id);
        return "redirect:/task-list";
    }
}
