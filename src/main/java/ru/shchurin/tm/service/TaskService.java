package ru.shchurin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.shchurin.tm.model.Task;
import ru.shchurin.tm.repository.TaskRepo;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Service
public final class TaskService {

    @Autowired
    TaskRepo taskRepo;

    @NotNull
    public Collection<Task> findAll() {
        return taskRepo.findAll();
    }

    @Nullable
    public Task findOne(@Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) return null;
        return taskRepo.findOne(taskId);
    }

    public void persist(@Nullable final Task task) {
        if (task == null) return;
        taskRepo.persist(task);
    }

    public void merge(@Nullable final Task task) {
        if (task == null) return;
        taskRepo.merge(task);
    }

    public void remove(@Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) return;
        taskRepo.remove(taskId);
    }

    public void removeAll() {
        taskRepo.removeAll();
    }

    public void removeByName(@Nullable final String name) {
        if (name == null) return;
        taskRepo.removeByName(name);
    }
}
