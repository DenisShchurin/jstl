package ru.shchurin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.shchurin.tm.model.Project;
import ru.shchurin.tm.repository.ProjectRepo;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

@Service
public final class ProjectService {

    @Autowired
    ProjectRepo projectRepo;

    @NotNull
    public Collection<Project> findAll() {
        return projectRepo.findAll();
    }

    @Nullable
    public Project findOne(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return projectRepo.findOne(projectId);
    }

    public void persist(@Nullable final Project project) {
        if (project == null) return;
        projectRepo.persist(project);
    }

    public void merge(@Nullable final Project project) {
        if (project == null) return;
        projectRepo.merge(project);
    }

    public void remove(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        projectRepo.remove(projectId);
    }

    public void removeAll() {
        projectRepo.removeAll();
    }

    public void removeByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return;
        projectRepo.removeByName(name);
    }
}
