
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>PROJECT-EDIT</title>
</head>
<body>
<h1>EDIT PROJECT</h1>

<form method="POST" action="/project-save">
    <table>
        <tr>
            <p>
                ID
            </p>
            <p>
                <input type="text" value="${project.id}" name="id">
            </p>
            <p>
                NAME
            </p>
            <p>
                <input type="text" value="${project.name}" name="name">
            </p>
            <p>
                DESCRIPTION
            </p>
            <p>
                <input type="text" value="${project.description}" name="description">
            </p>
            <p>
                DATE START
            </p>
            <p>
                <input type="date" value="${project.dateStart}" name="dateStart">
            </p>
            <p>
                DATE FINISH
            </p>
            <p>
                <input type="date" value="${project.dateFinish}" name="dateFinish">
            </p>
            <p>
                STATUS
            </p>
            <p>
                <select name="status">
                    <option value="STATUS_SCHEDULED">STATUS_SCHEDULED</option>
                    <option value="STATUS_IN_PROGRESS">STATUS_IN_PROGRESS</option>
                    <option value="STATUS_DONE">STATUS_DONE</option>
                </select>
            </p>
            <p>
                <input type="submit" value="SAVE PROJECT"/>
            </p>
    </table>
</form>
</body>
</html>
