<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>
<head>
    <title>PROJECTS</title>
</head>
<body>
    <h1>PROJECT MANAGER</h1>

    <div align="left">
        <table border="1" cellpadding="5">
            <tr>
                <th colspan="7" align="center">PROJECTS</th>
            </tr>
            <tr>
                <td>№</td>
                <td>ID</td>
                <td>Name</td>
                <td>DESCRIPTION</td>
                <td>VIEW</td>
                <td>EDIT</td>
                <td>REMOVE</td>
            </tr>
            <c:forEach var="project" items="${projects}" varStatus="status">
                <tr>
                    <td>${status.count}</td>
                    <td>${project.id}</td>
                    <td>${project.name}</td>
                    <td>${project.description}</td>
                    <td><a href="/project-view/${project.id}">VIEW</a> </td>
                    <td><a href="/project-edit/${project.id}">EDIT</a> </td>
                    <td><a href="/project-remove/${project.id}">REMOVE</a> </td>
                </tr>
            </c:forEach>
        </table>
    </div>
    <div>
        <form action="/project-create">
            <button type="submit">CREATE PROJECT</button>
        </form>
    </div>
    <div>
        <form action="/project-list">
            <button type="submit">REFRESH</button>
        </form>
    </div>
    <div>
        <form action="/task-list">
            <button type="submit">TASKS</button>
        </form>
    </div>

</body>
</html>
