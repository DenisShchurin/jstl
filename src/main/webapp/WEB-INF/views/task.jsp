<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>TASK</title>
</head>
<body>
    <table>
        <tr>
            <th width="120">ID</th>
            <th width="120">Name</th>
            <th width="120">DESCRIPTION</th>
            <th width="120">DATE START</th>
            <th width="120">DATE FINISH</th>
            <th width="120">STATUS</th>
            <th width="120">PROJECT ID</th>
        </tr>
        <tr>
            <td>${task.id}</td>
            <td>${task.name}</td>
            <td>${task.description}</td>
            <td>${task.dateStart}</td>
            <td>${task.dateFinish}</td>
            <td>${task.status}</td>
            <td>${task.projectId}</td>
        </tr>
</table>
</body>
</html>
