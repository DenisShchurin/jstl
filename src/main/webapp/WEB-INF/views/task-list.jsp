<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>TASKS</title>
</head>
<body>
<h1>PROJECT MANAGER</h1>

<div align="left">
    <table border="1" cellpadding="5">
        <tr>
            <th colspan="8" align="center">TASKS</th>
        </tr>
        <tr>
            <td>№</td>
            <td>ID</td>
            <td>Name</td>
            <td>DESCRIPTION</td>
            <td>PROJECT ID</td>
            <td>VIEW</td>
            <td>EDIT</td>
            <td>REMOVE</td>
        </tr>
        <c:forEach var="task" items="${tasks}" varStatus="status">
            <tr>
                <td>${status.count}</td>
                <td>${task.id}</td>
                <td>${task.name}</td>
                <td>${task.description}</td>
                <td>${task.projectId}</td>
                <td><a href="/task-view/${task.id}">VIEW</a> </td>
                <td><a href="/task-edit/${task.id}">EDIT</a> </td>
                <td><a href="/task-remove/${task.id}">REMOVE</a> </td>
            </tr>
        </c:forEach>
    </table>
</div>
<div>
    <form action="/task-create">
        <button type="submit">CREATE TASK</button>
    </form>
</div>
<div>
    <form action="/task-list">
        <button type="submit">REFRESH</button>
    </form>
</div>
</body>
</html>
