<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>CREATE PROJECT</title>
</head>
<body>
<h1>CREATE PROJECT</h1>

<form method="POST" action="/project-save">
    <table>
        <tr>
            <p>
                NAME
            </p>
            <p>
                <input type="text" name="name">
            </p>
            <p>
                DESCRIPTION
            </p>
            <p>
                <input type="text" name="description">
            </p>
            <p>
                DATE START
            </p>
            <p>
                <input type="date" name="dateStart">
            </p>
            <p>
                DATE FINISH
            </p>
            <p>
                <input type="date" name="dateFinish">
            </p>
            <p>
                STATUS
            </p>
            <p>
                <select name="status">
                    <option value="STATUS_SCHEDULED">STATUS_SCHEDULED</option>
                    <option value="STATUS_IN_PROGRESS">STATUS_IN_PROGRESS</option>
                    <option value="STATUS_DONE">STATUS_DONE</option>
                </select>
            </p>
            <p>
                <input type="submit" value="SAVE PROJECT"/>
            </p>
    </table>
</form>

</body>
</html>
