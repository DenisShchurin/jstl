<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>EDIT TASK</title>
</head>
<body>
<h1>EDIT TASK</h1>

<form method="POST" action="/task-save">
    <table>
        <tr>
            <p>
                ID
            </p>
            <p>
                <input type="text" value="${task.id}" name="id">
            </p>
            <p>
                NAME
            </p>
            <p>
                <input type="text" value="${task.name}" name="name">
            </p>
            <p>
                DESCRIPTION
            </p>
            <p>
                <input type="text" value="${task.description}" name="description">
            </p>
            <p>
                DATE START
            </p>
            <p>
                <input type="date" value="${task.dateStart}" name="dateStart">
            </p>
            <p>
                DATE FINISH
            </p>
            <p>
                <input type="date" value="${task.dateFinish}" name="dateFinish">
            </p>
            <p>
                STATUS
            </p>
            <p>
                <select name="status">
                    <option value="STATUS_SCHEDULED">STATUS_SCHEDULED</option>
                    <option value="STATUS_IN_PROGRESS">STATUS_IN_PROGRESS</option>
                    <option value="STATUS_DONE">STATUS_DONE</option>
                </select>
            </p>
            <p>
                PROJECT ID
            </p>
            <p>
                <input type="text" value="${task.projectId}" name="description">
            </p>
            <p>
                <input type="submit" value="SAVE TASK"/>
            </p>
    </table>
</form>
</body>
</html>
