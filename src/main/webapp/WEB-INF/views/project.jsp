
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Project</title>
</head>
<body>
    <table>
        <tr>
            <th width="120">ID</th>
            <th width="120">Name</th>
            <th width="120">DESCRIPTION</th>
            <th width="120">DATE START</th>
            <th width="120">DATE FINISH</th>
            <th width="120">STATUS</th>
        </tr>
        <tr>
            <td>${project.id}</td>
            <td>${project.name}</td>
            <td>${project.description}</td>
            <td>${project.dateStart}</td>
            <td>${project.dateFinish}</td>
            <td>${project.status}</td>
        </tr>
    </table>
</body>
</html>
